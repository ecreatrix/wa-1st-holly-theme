<?php
namespace App;

use function Roots\asset;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Helpers::class ) ) {
    class Helpers {
        public function __construct() {
        }

        public static function has_hero() {
            $post = get_post();

            if ( has_blocks( $post->post_content ) ) {
                return true;
            }

            return false;
        }

        public static function svg_or_file_return( $image_id, $alt = '' ) {
            $path = get_stylesheet_directory();
            $uri  = get_stylesheet_directory_uri();

            $file = wp_get_original_image_path( $image_id );

            if ( file_exists( $file ) ) {
                $filetype = mime_content_type( $file );

                if ( 'image/svg' === $filetype || 'image/svg+xml' === $filetype ) {
                    //Return svg file contents

                    return file_get_contents( $file );
                } else {
                    //Return img tag

                    return '<img src="' . wp_get_attachment_image_src( $image_id, 'full' )[0] . '" alt="' . $alt . '">';

                }
            }

            return '';
        }
    }

    new Helpers();
}