<?php
namespace App;

/**
 * Register the initial theme setup.
 *
 * @return void
 */
add_filter( 'admin_footer_text', function () {
    echo 'Built for you by <a href="https://withalyx.ca/" target="_blank">With Alyx</a> :)';
} );

add_action( 'customize_register', function ( $wp_customize ) {
    $wp_customize->add_setting( 'wa_footer_icon', [
        'default'           => '',
        'sanitize_callback' => 'esc_url_raw',
    ] );

    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'wa_footer_icon_control', [
        'label'         => 'Footer Icon',
        'priority'      => 20,
        'section'       => 'title_tagline',
        'settings'      => 'wa_footer_icon',
        'button_labels' => [ // All These labels are optional
            'select' => 'Select Icon',
            'remove' => 'Remove Icon',
            'change' => 'Change Icon',
        ],
    ] ) );
} );

add_action( 'after_setup_theme', function () {
    /**
     * Enable features from the Soil plugin if activated.
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support( 'soil', [
        'clean-up',
        'nav-walker',
        'nice-search',
        'relative-urls',
    ] );

    add_filter( 'body_class', function ( $classes ) {
        $classes[] = 'd-flex flex-column h-100';

        if ( Helpers::has_hero() ) {
            $classes[] = 'has-hero';
        }

        return $classes;
    } );

    /**
     * Disable full-site editing support.
     *
     * @link https://wptavern.com/gutenberg-10-5-embeds-pdfs-adds-verse-block-color-options-and-introduces-new-patterns
     */
    remove_theme_support( 'block-templates' );

    /**
     * Register the navigation menus.
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus( [
        'primary_navigation' => __( 'Primary Navigation', 'wa-theme' ),
        'footer_navigation'  => __( 'Footer Navigation', 'wa-theme' ),
    ] );

    /**
     * Disable the default block patterns.
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-the-default-block-patterns
     */
    remove_theme_support( 'core-block-patterns' );

    /**
     * Enable plugins to manage the document title.
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support( 'title-tag' );

    /**
     * Enable post thumbnail support.
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    /**
     * Enable responsive embed support.
     * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#responsive-embedded-content
     */
    add_theme_support( 'responsive-embeds' );

    add_theme_support( 'align-full' );

    /**
     * Enable HTML5 markup support.
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support( 'html5', [
        'caption',
        'comment-form',
        'comment-list',
        'gallery',
        'search-form',
        'script',
        'style',
    ] );

    /**
     * Enable selective refresh for widgets in customizer.
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support( 'customize-selective-refresh-widgets' );

    $defaults = [
        'height'               => 100,
        'width'                => 400,
        'flex-height'          => true,
        'flex-width'           => true,
        'header-text'          => ['site-title', 'site-description'],
        'unlink-homepage-logo' => true,
    ];

    add_theme_support( 'custom-logo', $defaults );
}, 20 );

/**
 * Register the theme sidebars.
 *
 * @return void

add_action( 'widgets_init', function () {
$config = [
'before_widget' => '<section class="widget %1$s %2$s">',
'after_widget'  => '</section>',
'before_title'  => '<h3>',
'after_title'   => '</h3>',
];

register_sidebar( [
'name' => __( 'Primary', 'wa-theme' ),
'id'   => 'sidebar-primary',
] + $config );

register_sidebar( [
'name' => __( 'Footer', 'wa-theme' ),
'id'   => 'sidebar-footer',
] + $config );
} );
 */