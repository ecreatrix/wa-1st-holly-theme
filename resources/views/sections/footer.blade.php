<footer class="footer jumbotron py-3 mt-auto"><div class="container d-flex flex-wrap">
	<div class="identity col-md-6">
		@if( get_site_icon_url( ) ) 
			<div class="logo">
				@if( get_theme_mod( 'wa_footer_icon' ) !== '' )
					{!! App\Helpers::svg_or_file_return( attachment_url_to_postid(get_theme_mod( 'wa_footer_icon' )), $siteName ) !!}
				@elseif( get_site_icon_url( ) !== '' )
					<img src="{!! get_site_icon_url( ) !!}" alt="{!! $siteName !!}">
				@elseif( has_custom_logo( ) ) 
					<img src="{!! wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' )[0] !!}" alt="{!! $siteName !!}">
				@endif
			</div>
		@endif
		
		<div class="copy">
			&copy; {{ date( 'Y' ) }} {!! $siteName !!}
		</div>
	</div>

	@if ( has_nav_menu( 'footer_navigation' ) )
		<div class="links col-md-3">
			{!! wp_nav_menu( ['theme_location' => 'footer_navigation', 'menu_class' => 'nav', 'echo' => false] ) !!}
		</div>
	@endif
</div></footer>
